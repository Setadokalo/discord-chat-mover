use std::{
	collections::HashMap, collections::HashSet, error::Error, fmt::Display,
	sync::Once, sync::RwLock, time::Duration,
};

use serde_json::{Value, json};
use serenity::{
	cache::FromStrAndCache,
	client::Context,
	client::EventHandler,
	framework::standard::help_commands,
	framework::standard::macros::command,
	framework::standard::macros::group,
	framework::standard::macros::help,
	framework::standard::Args,
	framework::standard::CommandError,
	framework::standard::CommandGroup,
	framework::standard::CommandResult,
	framework::standard::HelpOptions,
	framework::StandardFramework,
	model::channel::Channel,
	model::channel::Embed,
	model::channel::Message,
	model::id::ChannelId,
	model::id::UserId,
	model::webhook::Webhook,
	Client,
};
struct Handler;

impl EventHandler for Handler {}
#[group]
#[help_available]
#[commands(move_conversation, add_channel, list_hooked, kill_bot)]
struct General;

const CONFIG_PATH: &'static str = "config.json";

impl Drop for Handler {
	fn drop(&mut self) {
		save_config();
	}
}

fn save_config() {
	let hashmap: &HashMap<ChannelId, Webhook> = &*get_hooks().unwrap().read().unwrap();
	std::fs::write(
		CONFIG_PATH,
		serde_json::ser::to_string_pretty(hashmap)
			.expect("Failed to serialize config"),
	)
	.unwrap();
}
fn load_config() {
	let config = std::fs::read_to_string(CONFIG_PATH);
	let conf_hooks: HashMap<ChannelId, Webhook> = if let Ok(config) = config {
		println!("{}", config);
		serde_json::from_str(&*config).unwrap()
	} else {
		HashMap::new()
	};
	let _hooks = init_hooks(conf_hooks);
}
static mut HOOKS: Option<RwLock<HashMap<ChannelId, Webhook>>> = None;
static mut HOOKS_ONCE: Once = Once::new();
fn get_hooks() -> Option<&'static RwLock<HashMap<ChannelId, Webhook>>> {
	unsafe { HOOKS.as_ref() }
}
fn init_hooks(
	hooks: HashMap<ChannelId, Webhook>,
) -> &'static RwLock<HashMap<ChannelId, Webhook>> {
	unsafe {
		HOOKS_ONCE.call_once(|| {
			HOOKS = Some(RwLock::new(hooks));
		});
		HOOKS.as_ref().unwrap()
	}
}

#[derive(Debug)]
struct GenericError {
	error: Box<dyn Error>,
}
impl<T: 'static + Error> From<T> for GenericError {
	fn from(err: T) -> Self {
		Self {
			error: Box::new(err),
		}
	}
}
impl Display for GenericError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		self.error.fmt(f)
	}
}

fn main() {
	load_config();
	let mut client = Client::new(std::fs::read_to_string("client_token").expect("Failed to load token"), Handler).expect("Failed to get client");
	client.threadpool.set_num_threads(2);
	client.with_framework(
		StandardFramework::new()
			.configure(|c| c.prefix("--").allow_dm(false))
			.help(&MOVER_HELP)
			.unrecognised_command(|ctx, msg, unrecognised_command_name| {
				msg.channel(&ctx.cache).unwrap().id().say(&ctx.http, format!("Unknown command: {}", msg.content)).unwrap();
			})
			.on_dispatch_error(|ctx, msg, error| {
				msg.channel(&ctx.cache).unwrap().id().say(&ctx.http, format!("Command error: {:?}", error)).unwrap();
			})
			.after(|ctx, msg, cmd_name, error| {
				//  Print out an error if it happened
				if let Err(why) = error {
					msg.channel(&ctx.cache).unwrap().id().say(&ctx.http, format!("Error in {}: {:?}", cmd_name, why)).unwrap();
				}
			})
			// The `#[group]` (and similarly, `#[command]`) macro generates static instances
			// containing any options you gave it. For instance, the group `name` and its `commands`.
			// Their identifiers, names you can use to refer to these instances in code, are an
			// all-uppercased version of the `name` with a `_GROUP` suffix appended at the end.
			.group(&GENERAL_GROUP),
	);
	client.start().expect("Failed to start");
	println!("Bot startup complete!");
}

fn get_arguments(msg: &Message) -> Vec<&str> {
	let mut arguments: Vec<&str> = msg.content.split_whitespace().collect();
	arguments.remove(0); //the calling command; unneeded.
	arguments
}

fn get_channel(
	ctx: &mut Context,
	error_log_channel: Option<ChannelId>,
	channel_name: &str,
) -> Option<Channel> {
	let target_channel = Channel::from_str(&ctx.cache, channel_name);
	if let Ok(val) = target_channel {
		Some(val)
	} else {
		if let Some(elc) = error_log_channel {
			elc.say(&ctx.http, "Invalid channel!").unwrap();
		}
		None
	}
}

#[command]
#[num_args(1)]
#[usage = "<channel>"]
#[required_permissions("MANAGE_WEBHOOKS", "MANAGE_CHANNELS")]
#[description = "Adds a channel webhook to the bot, allowing it to move chat logs to said channel."]
#[aliases("addchannel", "ac")]
fn add_channel(ctx: &mut Context, msg: &Message) -> CommandResult {
	let mut hooks = get_hooks().unwrap().write().unwrap();
	let special_channel_name = get_arguments(msg)[0];
	let channel = if let Some(v) = get_channel(ctx, Some(msg.channel_id), special_channel_name) {v} else {
		return Err(CommandError("Failed to get valid channel".into()));
	};
	let channel_name = match &channel {
		Channel::Group(g) => {
			g.read().name.as_ref().unwrap().clone()
		}
		Channel::Guild(g) => {
			g.read().name.clone()
		}
		Channel::Private(pc) => {
			pc.read().name().clone()
		}
		Channel::Category(c) => {
			c.read().name.clone()
		}
		Channel::__Nonexhaustive => {unreachable!()}
	};
	if !hooks.contains_key(&channel.id()) {
		let map = json!({
			"name": channel_name
		});
		let hook = ctx.http.create_webhook(*channel.id().as_u64(), &map)?;
		//let Ok(_) = reqwest::blocking::get(hook) {
		hooks
			.insert(channel.id(), hook);
		drop(hooks);
		save_config();
		msg.channel_id.say(
			&ctx.http,
			format!("Added webhook for channel {}!", special_channel_name),
		)?;
		Ok(())
	} else {
		msg.channel_id.say(&ctx.http, "Channel already registered!")?;
		Err(CommandError("Channel already registered".into()))
	}
}


#[command]
#[description = "Lists the channels that have webhooks set up."]
#[aliases("listhooked", "list", "lh")]
fn list_hooked(ctx: &mut Context, msg: &Message) -> CommandResult {
	let hooks = get_hooks()
		.unwrap()
		.read()
		.expect("Failed to get read access");
	msg.channel_id.say(
		&ctx.http,
		format!("There are {} hooked channels: ", hooks.len()),
	)?;
	for hook in hooks.iter() {
		msg.channel_id.say(&ctx.http, hook.0)?;
	}

	Ok(())
}

//TODO: implement emoji-based message moving
#[command]
#[aliases("moveconversation", "move", "mc")]
#[required_permissions("MANAGE_MESSAGES", "SEND_MESSAGES")]
#[min_args(2)]
#[usage = "<channel> [emoji] <number of messages>"]
#[description = "Moves the specified amount of messages to the target channel. [UNIMPLEMENTED] If an emoji is specified, 
only messages with that emoji as a reaction will be moved, and the <number of messages> parameter will act as the number 
of messages to search for the emoji."]
fn move_conversation(ctx: &mut Context, msg: &Message) -> CommandResult {
	let arguments = get_arguments(msg);
	if arguments.len() < 2 {
		msg.channel_id
			.say(&ctx.http, "Invalid number of arguments!")?;
		return Err(CommandError("Invalid number of arguments".into()));
	}
	let channel_name = arguments[0];
	let hooks = get_hooks().unwrap();
	let channel = get_channel(ctx, Some(msg.channel_id), channel_name);
	if channel.is_none() {
		return Err(CommandError("Failed to get valid channel".into()));
	};
	if let Some(hook) = hooks
		.read()
		.expect("Failed to get read lock on hooks")
		.get(&channel.unwrap().id())
	{
		let messages_to_move: u64 = std::str::FromStr::from_str(arguments[1])?;
		// target_channel.id().say(ctx.http.clone(), "Testing!")?;
		let mut message_buffer = Vec::new();
		let mut messages = msg.channel_id.messages(&ctx.http, |retriever| {
			retriever.before(msg.id).limit(messages_to_move)
		})?;
		messages.reverse(); //we want to move the oldest first and the newest last; the iterator gives us the reverse
		for message in messages {
			message_buffer.push(create_embed_message(&message));
			if message_buffer.len() > 10 {
				panic!("Invalid message buffer length?!");
			}
			if message_buffer.len() == 10 {
				let mut buf = Vec::new();
				std::mem::swap(&mut message_buffer, &mut buf);
				send_message_buffer(ctx, &hook, buf)?;
			}
			// println!("{:?}", message.content);
			message.delete(&ctx.http)?;
		}

		if !message_buffer.is_empty() {
			send_message_buffer(ctx, &hook, message_buffer)?;
		}
		msg.delete(&ctx.http)?;

		let handle = msg.channel_id.say(&ctx.http, "Moved messages!")?;
		std::thread::sleep(Duration::from_secs_f64(5.0));
		handle.delete(&ctx.http)?;

		Ok(())
	} else {
		msg.channel_id.say(&ctx.http, format!("Channel does not have a webhook set up! Use !addchannel {} [webhook url] to set one up!", channel_name))?;
		Err(CommandError("Channel without hook".into()))
	}
}

fn send_message_buffer(
	ctx: &mut Context,
	hook: &Webhook,
	buffer: Vec<Value>,
) -> Result<(), GenericError> {
	hook.execute(&ctx.http, true, |w| w.embeds(buffer))?;
	Ok(())
	//, hook
}

fn create_embed_message(msg: &Message) -> Value {
	Embed::fake(|e| {
		e.author(|a| {
			if let Some(url) = msg.author.avatar_url() {
				a.icon_url(url);
			} else {
				a.icon_url("https://cdn.discordapp.com/embed/avatars/4.png");
			}
			a.name(msg.author.name.clone())
		});
		let mut description = msg.content.clone();
		for attachment in msg.attachments.iter() {
			description.push('\n');
			description.push_str(&*attachment.proxy_url);
		}
		e.description(description);
		e
	})
}

#[command]
#[required_permissions("ADMINISTRATOR")]
#[description = "Kills the bot. (will cause a panic bc I can't be fucked to use my library correctly"]
#[aliases("killbot", "kill")]
fn kill_bot(_ctx: &mut Context, _msg: &Message) -> CommandResult {
	panic!("Rust pls give me safe early out");
}

#[help]
fn mover_help(
	context: &mut Context,
	msg: &Message,
	args: Args,
	help_options: &'static HelpOptions,
	groups: &[&'static CommandGroup],
	owners: HashSet<UserId>,
) -> CommandResult {
	help_commands::with_embeds(context, msg, args, help_options, groups, owners)
}
